package LectureTwo;

public class Recursion {
    public static void main(String args[]) {
        reduceByOne(4);
        System.out.println(binaryToNumber("1011"));
        System.out.println(fib(3));
    }

    public static void reduceByOne(int n) {
        if( n > 0 ) {
            reduceByOne(n-1);
        }
        System.out.println(n);
        return;
    }

    public int powerN(int base, int n) {
        if (n == 1)
            return base;
        return base * powerN(base,n-1);
    }

    public static int fib(int n){
        if( n == 0) return 0;
        if (n == 1) return 1;
        return fib(n-1) + fib(n-2);
    }


    private static int binaryToNumber(String binaryNumber) {
        int decimal = 0;
        int length = binaryNumber.length();
        if (length > 0) {
            String substring = binaryNumber.substring(1);
            int digit = Character.getNumericValue(binaryNumber.charAt(0));
            decimal = digit * (int) Math.pow(2, length - 1) + binaryToNumber(substring);
        }
        return decimal;
    }


}
