package LectureThree;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Questions {
    public static void main(String args[]){
        System.out.println(reverseString("thisshouldbereversed"));
        convertToBinary(50);
        System.out.println();
        Questions q = new Questions();
        q.findPrimeNumbers(82);
        System.out.println();
        System.out.print(q.findFibNum(12));
        System.out.println();
        int[] arr = {4, 6, 5, 7, 8, 9, 2};
        q.sortUsingTwoStacks(arr);
        System.out.println("min");
        minValue(arr);
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(2);
        list.add(1);
        System.out.println(mirrors(list));
    }

    public static String reverseString(String str) {
        Stack stack = new Stack();
        for(int i = 0; i < str.length(); ++i)
            stack.push(str.charAt(i));
        String newString = "";
        while(!stack.empty()){
            newString += stack.pop();
        }
        return newString;
    }

    public static void convertToBinary(int n) {
        Stack stack = new Stack();
        while(n >0){
            stack.push(n%2);
            n /= 2;
        }
        while(!stack.empty()) {
            System.out.print(stack.pop());
        }
    }

    public boolean isPrime(int n) {
        for(int i = 2; i < 10; ++i) {
            if(n == i) {

            } else if( n == 1) {
                return true;
            } else if(n % i == 0) {
                return false;
            }
        }
        return true;
    }

    public void findPrimeNumbers(int n) {
        int i = 1;
        while (i < n ) {
            boolean isPrime = isPrime(i);
            if(isPrime) {
                System.out.print(i + ", ");
            }
            i++;
        }
    }

    public boolean findFibNum(int n) {
        int a = 0;
        int b = 1;
        int c = 1;
        while(c <= n){
            if( c == n )
                return true;
            a = b;
            b = c;
            c = a + b;
        }
        return false;
    }

    public static void minValue(int[] arr) {
        Stack<Integer> stack1 = new Stack();
        for(int i = 0; i < arr.length; ++i) {
            if(stack1.isEmpty()){
                stack1.push(arr[i]);
            } else {
                if( arr[i] < stack1.peek()){
                    stack1.push(arr[i]);
                }
            }
        }
        while(!stack1.isEmpty()){
            System.out.println(stack1.pop());
        }
    }

    public void sortUsingTwoStacks(int[] arr) {
        Stack<Integer> stack1 = new Stack();
        Stack<Integer> stack2 = new Stack();

        for(int i = 0; i < arr.length; ++i) {
            if(stack1.isEmpty()){
                stack1.push(arr[i]);
            } else {
                if( arr[i] < stack1.peek()){
                    stack1.push(arr[i]);
                } else {
                    while(stack1.size() > 0 && arr[i] > stack1.peek()){
                        stack2.push(stack1.pop());
                    }
                    stack1.push(arr[i]);
                    while(!stack2.isEmpty()){
                        stack1.push(stack2.pop());
                    }
                }
            }
        }
        while(!stack1.isEmpty()){
            System.out.println(stack1.pop());
        }

    }

    public static boolean mirrors(List list) {
        if(list.size() == 0 || list.size() == 1)
            return true;

        if(list.get(0) == list.get(list.size()-1)) {
            list.remove(0);
            list.remove(list.size()-1);
            return mirrors(list);
        }
        return false;
    }

    public static int binarySearch(int arr[], int left, int right, int element)
    {
        if (right>=left)
        {
            int mid = left + (right - left)/2;

            if (arr[mid] == element)
                return mid;

            if (arr[mid] > element)
                return binarySearch(arr, left, mid-1, element);

            return binarySearch(arr, mid+1, right, element);
        }

        return -1;
    }
}
